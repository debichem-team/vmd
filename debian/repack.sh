#!/bin/bash

SRC=../vmd-1.9.1.src.tar.gz
test -n "$1" && SRC=$1

TMPDIR=$(mktemp --directory)

cp $SRC $TMPDIR/
(
    cd $TMPDIR

    # unpack the original source tree
    tar -xf $(basename $SRC)
    rm -f $(basename $SRC)
    mv vmd* vmd
    mv plugins vmd/plugins_source

    # remove CVS directories from sources
    find vmd/ -name CVS | xargs rm -rf
    # remove CVS conflict copy
    find vmd/ -name '.#*' -delete
    # remove prebuilt binaries
    find vmd/ -name '*.dll' -delete
    find vmd/ -name '*.o'   -delete
    find vmd/ -name '*.so'  -delete
    # the following files are gone in vmd-1.9.3, thus '-f' option is used
    rm -f vmd/plugins_source/fmtool/fmtool.gcc
    rm -f vmd/plugins_source/fmtool/fmtool.icc
    rm -f vmd/plugins_source/fmtool/fmtool
    rm vmd/lib/points/distribute

    # pack it up again, but as xz
    tar -cJf $(basename $SRC .gz | sed 's/^vmd-/vmd_/; s/src/orig/').xz vmd
)

# back to origin and cleanup
mv $TMPDIR/*.xz ..
rm -rf $TMPDIR
